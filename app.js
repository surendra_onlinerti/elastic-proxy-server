var express = require('express');
var bodyParser = require('body-parser');


var index = require('./routes/index');

var app = express();


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true,parameterLimit:50000}));

app.use('/', index);

module.exports = app;
