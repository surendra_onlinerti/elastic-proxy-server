var express = require('express');
var router = express.Router();
var dataCompare = require('./dataCompare.js')


router.get('/executeQuery',(req,res)=>{
    var searchType = req.param('searchType', null);
    var index = req.param('index', null);
    var query = req.param('query' , null);
    console.log(searchType,index);
    if(searchType != null) searchType = searchType.toLowerCase();
    dataCompare.getElasticSearchCount(searchType,index,query,function(result){
        res.status(200).send((result).toString());
    })
})

module.exports = router;
