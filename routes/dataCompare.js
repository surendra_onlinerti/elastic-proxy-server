require('dotenv').config();
var elasticsearch = require('elasticsearch');
const { json } = require('body-parser');

var url = 'http://'+process.env.userName+':'+process.env.password+'@'+process.env.host+':'+process.env.port+'';

var client = new elasticsearch.Client({
    host: url
})

var getElasticSearchCount = function(searchType,index,query,cb){
     if(searchType == 'count' && index != null) {
        client.count({
            index: index
        }).then((result)=>{
            console.log(result.count);
            return cb(result.count)
        }).catch((err)=>{
            console.log(err);
            return cb(err.message);
        })
     }
     else if(searchType == 'search' && index !=null && query != null){
         client.search({
             index:index,
             type: 'document',
             body: query
         }).then((result)=>{
            var result = JSON.stringify(result.aggregations.status_aggs.buckets);
            return cb(result);
         }).catch((err)=>{
             console.log(err);
             return cb(err.message);
         })
     }else {
        if(index == null) return cb("index is not correct");
        else if(searchType == null || (searchType != 'count' && searchType != 'search')) return cb("searchType is not correct");
        else return cb("query is not correct"); 
     }
}

module.exports.getElasticSearchCount = getElasticSearchCount;
